﻿var path = require('path');
var publicPath="./";
var webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./base.js');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var Dashboard = require('webpack-dashboard');
var DashboardPlugin = require('webpack-dashboard/plugin');
var dashboard = new Dashboard();

module.exports = function(env){
	return webpackMerge(commonConfig(),{
		entry: {
			'entry':'./src/main.js'
		},
		devtool: "eval-source-map",
		watchOptions:{
			ignored: /node_modules/
		},
		devServer: {
			hot:true,
			port: 7777,
			host: 'localhost',
			contentBase:publicPath,
			watchContentBase: true,
			publicPath: publicPath,
			compress: true,
			inline:true,
			stats: {
				colors: true
			}
		},
		plugins:[
			new webpack.optimize.UglifyJsPlugin({
				sourceMap: true
			}),
			new webpack.DefinePlugin({
				'process.env.NODE_ENV': JSON.stringify('develpments')
			}),
			new webpack.HotModuleReplacementPlugin(),
			new webpack.NamedModulesPlugin(),
			new DashboardPlugin(dashboard.setData)
		]
	});
};
