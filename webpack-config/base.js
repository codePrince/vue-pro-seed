var path = require('path');
var publicPath="/";
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = function() {
	return {
		entry: './src/entry.js',
		output: {
			path: path.join(__dirname, '/../dist/assets'),
			filename: '[name].bundle.js',
			publicPath: publicPath,
			sourceMapFilename: '[name].map'
		},
		resolve: {
			alias: {
				'vue$': 'vue/dist/vue.common.js'
			},
			extensions: ['.js','.vue', '.json',".less"],
			modules: [path.join(__dirname, 'src'), 'node_modules']
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					loader: 'babel-loader',
					exclude: /node_modules/
				},
				{
					enforce: 'pre',
					test: /\.vue$/,
					loader: "vue-loader"
				},
				{
					test: /\.css$/,
					loader: ExtractTextPlugin.extract({
						fallback: "style-loader",
						use:['css-loader']
					})
				},
				{
					test : /\.less$/,
					loader: ExtractTextPlugin.extract({
						fallback: "style-loader",
						use:['css-loader','less-loader']
					})
				},
				{
					test: /\.(jpg|png|gif)$/,
					use: 'file-loader'
				},
				{
					test: /\.(woff|woff2|eot|ttf|svg)$/,
					use: {
						loader: 'url-loader',
						options: {
							limit: 100000
						}
					}
				},
				{
					test: /\.html$/,
					use: 'html-loader'
				}
			]},
		plugins: [
			new ExtractTextPlugin({
				filename: "bundle.css",
				disable: false,
				allChunks: true
			}),
			new HtmlWebpackPlugin({
				template: 'index.html',
				chunksSortMode: 'dependency'
			})
		]
	}
}
