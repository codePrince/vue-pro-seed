﻿var webpack = require('webpack');
var webpackConfig = require("./webpack-config/dev.js");

var WebpackDevServer = require("webpack-dev-server");
var compiler = webpack(webpackConfig("dev"));
var host="localhost";
var port=7000;

var server = new WebpackDevServer(compiler,{
	stats: {
		colors: true
	},
	quiet: true
});
server.listen(port, host, function() {
	console.log('Example app listening at http://%s:%s', host, port);
});
